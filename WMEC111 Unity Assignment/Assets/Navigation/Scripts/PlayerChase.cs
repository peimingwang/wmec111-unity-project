﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerChase : MonoBehaviour {
    
    //creates spot range variable for you to muck around with in the inspector
    public int SpotRange;
    //creates a place for the navmesh component added via scene editor and the inspector. 
    private NavMeshAgent agent;

    // Use this for initialization
    void Start () {
        //retrieves the navmesh agent component and assigns it to the space we made earlier
        agent = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update() {
        //find the players position
        Transform player = GameObject.FindGameObjectWithTag("Player").transform;
        //work out the distance from the player
        var distance = Vector3.Distance(player.position, transform.position);

        //if the distance is less than what was assigned to spot range
        if (distance <= SpotRange)
        {
            //make the navmesh agents goal the player's current position. 
            agent.destination = player.position;
        }

        else
        {

        }
    }
}
